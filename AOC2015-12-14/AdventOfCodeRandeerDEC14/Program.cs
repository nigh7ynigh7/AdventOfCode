﻿using System;
using System.Collections.Generic;


/*
--- Day 14: Reindeer Olympics ---

This year is the Reindeer Olympics! Reindeer can fly at high speeds, but must rest occasionally to recover their energy. Santa would like to know which of his reindeer is fastest, and so he has them race.

Reindeer can only either be flying (always at their top speed) or resting (not moving at all), and always spend whole seconds in either state.

For example, suppose you have the following Reindeer:

    Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
    Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.

After one second, Comet has gone 14 km, while Dancer has gone 16 km. After ten seconds, Comet has gone 140 km, while Dancer has gone 160 km. On the eleventh second, Comet begins resting (staying at 140 km), and Dancer continues on for a total distance of 176 km. On the 12th second, both reindeer are resting. They continue to rest until the 138th second, when Comet flies for another ten seconds. On the 174th second, Dancer flies for another 11 seconds.

In this example, after the 1000th second, both reindeer are resting, and Comet is in the lead at 1120 km (poor Dancer has only gotten 1056 km by that point). So, in this situation, Comet would win (if the race ended at 1000 seconds).

Given the descriptions of each reindeer (in your puzzle input), after exactly 2503 seconds, what distance has the winning reindeer traveled?

--- Part Two ---

Seeing how reindeer move in bursts, Santa decides he's not pleased with the old scoring system.

Instead, at the end of each second, he awards one point to the reindeer currently in the lead. (If there are multiple reindeer tied for the lead, they each get one point.) He keeps the traditional 2503 second time limit, of course, as doing otherwise would be entirely ridiculous.

Given the example reindeer from above, after the first second, Dancer is in the lead and gets one point. He stays in the lead until several seconds into Comet's second burst: after the 140th second, Comet pulls into the lead and gets his first point. Of course, since Dancer had been in the lead for the 139 seconds before that, he has accumulated 139 points by the 140th second.

After the 1000th second, Dancer has accumulated 689 points, while poor Comet, our old champion, only has 312. So, with the new scoring system, Dancer would win (if the race ended at 1000 seconds).

Again given the descriptions of each reindeer (in your puzzle input), after exactly 2503 seconds, how many points does the winning reindeer have?

*/
namespace AdventOfCodeRandeerDEC14
{
	class Raindeer {
		public int Speed{ get; set; }
		public string Name { get; set; }
		private int RestingLim { get; set; }
		private int RunningLim { get; set; }
		private int ElapsedTimeState { get; set; }
		private bool IsRunning { get; set; }
		private int DistancedTraveled { get; set; }
		private int Point{ get; set; }
		public Raindeer(string Name, int Speed, int RestingLim, int RunningLim){
			this.Name = Name;
			this.Speed = Speed;
			this.RestingLim = RestingLim;
			this.RunningLim = RunningLim;
			ElapsedTimeState = 0;
			DistancedTraveled = 0;
			IsRunning = true;
			Point = 0;
		}

		public void Process(){
			if (IsRunning) {
				DistancedTraveled += Speed;
				ElapsedTimeState++;
				if (ElapsedTimeState >= RunningLim) {
					ElapsedTimeState = 0;
					IsRunning = false;
				}
			} else {
				ElapsedTimeState++;
				if (ElapsedTimeState >= RestingLim) {
					ElapsedTimeState = 0;
					IsRunning = true;
				}
			}
		}
		public void AddExtraPoint(){
			Point++;
		}

		public int GetDistance(){
			return DistancedTraveled;
		}

		public int GetPoints(){
			return Point;
		}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			var raindeers = new List<Raindeer> ();
//			raindeers.Add (new Raindeer ("Comet", 14, 127, 10));
//			raindeers.Add (new Raindeer ("Dancer", 16, 162, 11));

//			Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.
			raindeers.Add (new Raindeer ("Vixen", 19, 124, 7));
//			Rudolph can fly 3 km/s for 15 seconds, but then must rest for 28 seconds.
			raindeers.Add (new Raindeer ("Rudolph", 3, 28, 15));
//			Donner can fly 19 km/s for 9 seconds, but then must rest for 164 seconds.
			raindeers.Add (new Raindeer ("Donner", 19, 164, 9));
//			Blitzen can fly 19 km/s for 9 seconds, but then must rest for 158 seconds.
			raindeers.Add (new Raindeer ("Blitzen", 19, 158, 9));
//			Comet can fly 13 km/s for 7 seconds, but then must rest for 82 seconds.
			raindeers.Add (new Raindeer ("Comet", 13, 82, 7));
//			Cupid can fly 25 km/s for 6 seconds, but then must rest for 145 seconds.
			raindeers.Add (new Raindeer ("Cupid", 25, 145, 6));
//			Dasher can fly 14 km/s for 3 seconds, but then must rest for 38 seconds.
			raindeers.Add (new Raindeer ("Dasher", 14, 38, 3));
//			Dancer can fly 3 km/s for 16 seconds, but then must rest for 37 seconds.
			raindeers.Add (new Raindeer ("Dancer", 3, 37, 16));
//			Prancer can fly 25 km/s for 6 seconds, but then must rest for 143 seconds.
			raindeers.Add (new Raindeer ("Prancer", 25, 143, 6));

			const int limit = 2503;
//			const int limit = 1000;

//			for (int i = 0; i < limit; i++) {
//				for (int k = 0; k < raindeers.Count; k++) {
//					raindeers [k].Process ();
//				}
//			}
			for (int i = 0; i < limit; i++) {
				int leadDist = 0;
				//run
				for (int k = 0; k < raindeers.Count; k++) {
					raindeers [k].Process ();
					if (raindeers [k].GetDistance () > leadDist)
						leadDist = raindeers [k].GetDistance();
				}
				//check lead
				for (int k = 0; k < raindeers.Count; k++) {
					if (raindeers[k].GetDistance() == leadDist) {
						raindeers [k].AddExtraPoint ();
					}
				}

			}

			Raindeer winner = null;
//			int distance = 0;
//			foreach (var raindeer in raindeers) {
//				if (raindeer.GetDistance () > distance) {
//					winner = raindeer;
//					distance = raindeer.GetDistance ();
//				}
//			}
			int points = 0;
			foreach (var raindeer in raindeers) {
				if (raindeer.GetPoints () > points) {
					winner = raindeer;
					points = raindeer.GetPoints ();
				}
			}
			if (winner == null) {
				winner = new Raindeer ("-", 0, 0, 0);
			}
			Console.WriteLine ("Winning raindeer, " + winner.Name + 
				", traveled " + winner.GetDistance() +
				" with " + winner.GetPoints() + "pts.");

			foreach (var item in raindeers) {
				Console.WriteLine (item.Name + item.GetDistance()+";"+item.GetPoints());
			}

			Console.ReadKey ();
		}
	}
}
