﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AdventOfCodeDEC15
{
	class Ingredient{
		public string Name { get; set; }
		public int Capacity { get; set; }
		public int Durability { get; set; }
		public int Flavor { get; set; }
		public int Texture { get; set; }
		public int Calories { get; set; }
		public Ingredient (string name)
		{
			this.Name = name;
		}
	}
	class Cookies {
		public List<Ingredient> Ingredients {get; set; }
		public Cookies(){
			LoadIngredients ();
		}
		public void LoadIngredients(){
			var ingredients = new List<Ingredient> ();
			try{
				var open = File.OpenText("Input.txt") as StreamReader;
				while(true){
					string read = open.ReadLine();
					string expression = String.Format(@"(\w+): capacity {0}, durability {0}, flavor {0}, texture {0}, calories {0}", @"([-]{0,1}\d+)");
					var m = Regex.Match(read, expression).Groups;

					var i = new Ingredient(m[1].Value);
					int num = 0;
					int.TryParse(m[2].Value,out num); i.Capacity = num;
					int.TryParse(m[3].Value,out num); i.Durability = num;
					int.TryParse(m[4].Value,out num); i.Flavor = num;
					int.TryParse(m[5].Value,out num); i.Texture = num;
					int.TryParse(m[6].Value,out num); i.Calories = num;

					ingredients.Add(i);

					if(open.EndOfStream){
						break;
					}
				}
			}catch(Exception){
				Console.WriteLine ("Input not read");
			}
			Ingredients = ingredients;
		}
		public int MaxScore { get; set; }
		public void CalculateIngredients(List<Ingredient> ingredients, int passes, int[] sum){
			if (sum == null || sum.Length!=4) 
				sum = new int[4] {0,0,0,0};
			if (ingredients.Count== 0) {
				int result = 1;

				foreach (var item in sum) {
					if (item <= 0)
						result *= 0;
					else
						result *= item; 
				}
				if (MaxScore < result) {
					MaxScore = result;
				}
				return;
			}
			for (int i = 1; i <= passes; i++) {
				var array = new int[] { 
					sum[0] + ingredients [0].Capacity * i,
					sum[1] + ingredients [0].Durability * i,
					sum[2] + ingredients [0].Flavor * i,
					sum[3] + ingredients [0].Texture * i
				};

				CalculateIngredients (ingredients.GetRange(1, ingredients.Count-1), passes - i, array);
			}
		}

		private void CalculateIngredients(List<Ingredient> ingredients, int passes, int[] sum, int limitcal, int sumcal){
			if (sum == null || sum.Length!=4) 
				sum = new int[4] {0,0,0,0};
			
			if (ingredients.Count== 0) {
				int result = 1;
				foreach (var item in sum) {
					if (item <= 0)
						result *= 0;
					else
						result *= item; 
				}
				if (MaxScore < result && sumcal == limitcal) {
					MaxScore = result;
				}
				return;
			}
			for (int i = 1; i <= passes; i++) {
				var array = new int[] { 
					sum[0] + ingredients [0].Capacity * i,
					sum[1] + ingredients [0].Durability * i,
					sum[2] + ingredients [0].Flavor * i,
					sum[3] + ingredients [0].Texture * i
				};
				var total = sumcal + ingredients [0].Calories * i;

				CalculateIngredients (ingredients.GetRange(1,ingredients.Count-1), passes - i, array, limitcal, total);
			}
		}
		public void CalculateIngredients(List<Ingredient> ingredients, int passes, int[] sum, int limitcal){
			CalculateIngredients(ingredients, 100, sum, limitcal, 0);
		}
	}
	class MainClass
	{	

		public static void Main (string[] args)
		{
			
			var cookies = new Cookies ();
//			cookies.CalculateIngredients (cookies.Ingredients, 100, null);
//			Console.WriteLine (cookies.MaxScore + " Is the max score here (no cal limit)");
//
//			//reset scores
//			cookies.MaxScore = 0;

			cookies.CalculateIngredients (cookies.Ingredients, 100, null, 500);
			Console.WriteLine (cookies.MaxScore + " Is the max score here with 500 cal restriction");
			Console.ReadKey ();

		}
	}
}
