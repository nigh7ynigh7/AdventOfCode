﻿using System;
using System.IO;

namespace AOCDEC01
{
	class MainClass
	{
		public static int Value(string s){
			int counter = 0;
			for (int i = 0; i < s.Length; i++) {
				switch (s[i]) {
					case '(':
						counter++;
						break;
					case ')':
						counter--;
						break;
					default:
						break;
				}
			}
			return counter;
		}
		public static int MagicValue(string s){
			int floor = 0;
			int index = 0;
			for (int i = 0; i < s.Length; i++) {
				if (s [i] == '(')
					floor++;
				else if (s [i] == ')')
					floor--;
				
				if (floor == -1) {
					index = i+1; // +1 because fuck me, it was supposed to start at 1.
					break;
				}
			}
			return index;
		}
		public static string Read(){
			try{
				var file = File.OpenText(@"Input.txt") as StreamReader;
				return file.ReadToEnd ();
			}catch(Exception e){
				Console.WriteLine (e.Message);
			}
			return "";
		}
		public static void Main (string[] args)
		{
			var input = Read();
//			var answer = Value(input);
//			Console.WriteLine (answer);
			var answer = MagicValue(input);
			Console.WriteLine (answer);
			Console.ReadKey ();
		}
	}
}
