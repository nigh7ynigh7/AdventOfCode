﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AOCDEC02
{
	class Box{
		public int Length { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }

		public Box (int length, int width, int height)
		{
			this.Length = length;
			this.Width = width;
			this.Height = height;
		}

		public int CalculateWrappingArea(){
			return 2 * Length * Width +
				2 * Width * Height +
				2 * Height * Length;
		}

		public int CalculateSmallCut(){
			int l = Length, h = Height, w = Width;
			int answer = l * h * w;
			var arr = new int[] { l, w, h };
			for (int i = 0; i < arr.Length; i++) {
				for (int j = i+1; j < arr.Length; j++) {
					int square = arr [i] * arr [j];
					if (answer > square) {
						answer = square;
					}
				}
			}
			return answer;
		}

		public int CalculateSmallestPerimeter(){
			int l = Length, h = Height, w = Width;
			int answer = 2 * l + 2* h  + 2 * w;
			var arr = new int[] { l, w, h };
			for (int i = 0; i < arr.Length; i++) {
				for (int j = i+1; j < arr.Length; j++) {
					int perimeter = arr [i] * 2 + 2 * arr [j];
					if (answer > perimeter) {
						answer = perimeter;
					}
				}
			}

			return answer;
		}

		public int CalculateRibbonRequirements(){
			return CalculateSmallestPerimeter () + CalculateVolume ();
		}

		public int CalculateVolume(){
			return Length * Width * Height;
		}
	}
	class MainClass
	{
		public static List<Box> Load(){
			var boxes = new List<Box> ();
			try {
				var file = File.OpenText(@"Input.txt") as StreamReader;
				while (true) {
					string line = file.ReadLine();
					string expression = @"(\d+)x(\d+)x(\d+)";

					var matches = Regex.Match(line, expression).Groups;
					boxes.Add(
						new Box(
							int.Parse(matches[1].Value),
							int.Parse(matches[2].Value),
							int.Parse(matches[3].Value)));

					if(file.EndOfStream)
						break;
				}
			} catch (Exception ex) {
				Console.WriteLine (String.Format("Error: {0}\n Stack trace: {1}",ex.Message, ex.StackTrace));
			}
			return boxes;
		}
		public static void Main (string[] args)
		{
			var boxes = Load ();
			int total = 0;
			int ribbons = 0;
			foreach (var box in boxes) {
				total += box.CalculateWrappingArea () + box.CalculateSmallCut ();
				ribbons += box.CalculateRibbonRequirements ();
			}
			Console.WriteLine ("They'll need {0} in total for wrapping and {1} for ribbons", total, ribbons);
			Console.ReadKey ();

		}
	}
}
