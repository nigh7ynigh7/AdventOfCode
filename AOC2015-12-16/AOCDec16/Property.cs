﻿using System;

namespace AOCDec16
{
	public class Property
	{
		public string Name { get; set; }
		public int Value { get; set; }
		public Property (string name, int value)
		{
			Name = name;
			Value = value;

		}

		public bool Compares (object obj)
		{
			var p = obj as Property;
			bool check = true;

			if (Name != p.Name)
				check = false;
			if (Value != p.Value)
				check = false;

			return check;
		}

		public bool CompareName(object obj){
			var p = obj as Property;
			bool check = true;
			if (Name != p.Name)
				check = false;
			return check;
		}

		public bool IsValueGreater(Property p){
			if (p.Name != Name)
				return false;
			if (p.Value > Value)
				return true;
			return false;
		}

		public bool IsValueLesser(Property p){
			if (p.Name != Name)
				return false;
			if (p.Value < Value)
				return true;
			return false;
		}
	}
}

