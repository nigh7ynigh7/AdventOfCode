﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AOCDec16
{
	class MainClass
	{
		public static List<Person> LoadList(){
			var list = new List<Person> ();

			try{
				var file = File.OpenText(@"Input\Input.txt") as StreamReader;
				while(true){
					string s = file.ReadLine();

					string name = s.Substring(0,s.IndexOf(":"));
					var p = new Person(name);

					string properties = s.Substring(s.IndexOf(":")+1);
					string expression = @"(\w+): (\d+)";

					var matches = Regex.Matches(properties, expression);

					foreach (Match match in matches) {
						var g = match.Groups;
						p.AddProperty (g[1].Value, int.Parse(g[2].Value));
					}

					list.Add(p);

					if (file.EndOfStream) {
						break;
					}
				}
			}catch(Exception e){
				Console.WriteLine ("Error: " + e.Message);
			}

			return list;
		}
		public static void Main (string[] args)
		{
			var people = LoadList ();

			var aunt = new Person ("Aunt Sue");
			aunt.AddProperty ("children", 3);
			aunt.AddProperty ("cats", 7);
			aunt.AddProperty ("samoyeds", 2);
			aunt.AddProperty ("pomeranians", 3);
			aunt.AddProperty ("akitas", 0);
			aunt.AddProperty ("vizslas", 0);
			aunt.AddProperty ("goldfish", 5);
			aunt.AddProperty ("trees", 3);
			aunt.AddProperty ("cars", 2);
			aunt.AddProperty ("perfumes", 1);

			Person suspect = null;
			int i = 0;


			foreach (var sue in people) {
				int score = aunt.MatchRange (sue);
				if (i < score) {
					i = score;
					suspect = sue;
				}
			}

			if (suspect == null)
				suspect = new Person ("");

			Console.WriteLine (suspect.Name);

			Console.ReadKey ();
		}
	}
}
