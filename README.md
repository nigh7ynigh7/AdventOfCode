# About this whole endeavor

Just got out of college and I wanted to improve my skills. So I started to do the AOC in MONO/C#.

Why MONO/C#? Because I want to learn C# and I'm about to do the S2B course they're going to hold somewhere in this country (Brazil). Nothing much aside from that.

To open these projects, pelase use Xamarin Studio or Monodevelop. Thanks!

Check it out >[here](http://adventofcode.com/)<.

# Special notes about some solutions

Although I did most of them with some relative ease, there were a few solutions that really tripped me up.

## 2015 DEC 15

This problem got me hard. I was able to do the first part by using a recursive method to "brute-force" the answer.

Although the second part was based on the calories (limit calories to 500) and was supposed to follow the same logic as the first part, my solution never got me the right answer. So yeah =/

I got help here!
